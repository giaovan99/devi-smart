// CAROUSEL
$(document).ready(function () {
  // Demo Carousel
  $(".carousel").slick({
    centerMode: true,
    centerPadding: "300px",
    slidesToShow: 1,
    autoplay: true,
    autoplaySpeed: 2500,
    infinity: true,
    adaptiveHeight: true,
    zIndex: 20,
    responsive: [
      {
        breakpoint: 1399.98,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: "200px",
          slidesToShow: 1,
        },
      },
      {
        breakpoint: 991.98,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: "100px",
          slidesToShow: 1,
        },
      },
      {
        breakpoint: 575.98,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: "0px",
          slidesToShow: 1,
          autoplaySpeed: 2000,
        },
      },
    ],
  });
  // feedbackCarousel
  $(".feedbackCarousel").slick({
    slidesToShow: 3,
    adaptiveHeight: true,
    autoplay: false,
    responsive: [
      {
        breakpoint: 991.98,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: "150px",
          slidesToShow: 1,
          autoplay: true,
          adaptiveHeight: false,
          infinity: true,
        },
      },
      {
        breakpoint: 767.98,
        settings: {
          centerMode: true,
          centerPadding: "0px",
          slidesToShow: 1,
          autoplay: true,
          adaptiveHeight: false,
          infinity: true,
        },
      },
      {
        breakpoint: 430.98,
        settings: {
          centerMode: true,
          centerPadding: "0px",
          slidesToShow: 1,
          autoplay: true,
          adaptiveHeight: false,
          infinity: true,
        },
      },
    ],
  });
});

// STICKY
window.onscroll = function () {
  stickyFunction();
};

var deviSmart = document.getElementById("deviSmart");
var deviSmartHeight = Math.abs(deviSmart.offsetTop);
var contactFormSticky = document.getElementById("contactFormSticky");

function stickyFunction() {
  // sticky rightSide (Nhận tư vấn ngay)
  if (window.pageYOffset > deviSmartHeight) {
    contactFormSticky.classList.add("fixed-bottom");
    contactFormSticky.classList.remove("d-none");
  } else {
    contactFormSticky.classList.remove("fixed-bottom");
    contactFormSticky.classList.add("d-none");
  }
  // sticky leftSide (hotline) on mobile
  if (window.pageXOffset <= 576) {
    stickyPhone.classList.add("fixed-bottom");
    stickyPhone.classList.remove("d-none");
  } else {
    stickyPhone.classList.remove("fixed-bottom");
    stickyPhone.classList.add("d-none");
  }
}

// VALIDATION FORM
// check rỗng
function isEmpty(value, warningId) {
  if (value == "") {
    document.getElementById(warningId).innerText = "Vui lòng không để trống";
    return false;
  } else {
    document.getElementById(warningId).innerText = "";
    return true;
  }
}
// check phone
function isPhone(phone, warningId) {
  if (isEmpty(phone, warningId)) {
    let regex = /^(?!.*[^\d+])(84|0[35789])(\d{8})\b/;
    if (phone.match(regex)) {
      document.getElementById(warningId).innerText = "";
      return true;
    } else {
      document.getElementById(warningId).innerText =
        "Số điện thoại không hợp lệ";
      return false;
    }
  } else {
    return false;
  }
}
function validateForm(event) {
  event.preventDefault();
  let check = true,
    url =
      "https://script.google.com/macros/s/AKfycbwi4-IRxujTyPbjyKR5G-nSdg9WIG_Bx2kN-mb65N0qgaDPXEzG-VaAyIxGnSsfFa9n/exec",
    $form = $("form#sendContactForm");
  // lấy dữ liệu từ form
  let fullName = document.getElementById("fullName").value;
  console.log(fullName);
  let phoneNumber = document.getElementById("phoneNumber").value;
  let spaName = document.getElementById("spaName").value;
  let spaAddress = document.getElementById("spaAddress").value;
  // check rỗng
  check =
    check &
    isEmpty(fullName, "fullNameWarning") &
    isPhone(phoneNumber, "phoneNumberWarning") &
    isEmpty(spaName, "spaNameWarning") &
    isEmpty(spaAddress, "spaAddressWarning");
  console.log(check);

  // gửi phản hồi & reset form & gửi data lên gg sheet
  if (check) {
    var jqxhr = $.ajax({
      url: url,
      method: "POST",
      dataType: "json",
      data: $form.serialize(),
    }).success(
      (document.getElementById("formConfirm").innerText =
        "Đăng ký thành công, bạn sẽ nhận được thông tin liên hệ từ Devitech")
    );

    document.getElementById("sendContactForm").reset();
  }
}
